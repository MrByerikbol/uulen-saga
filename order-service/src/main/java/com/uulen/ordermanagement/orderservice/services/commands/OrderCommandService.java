package com.uulen.ordermanagement.orderservice.services.commands;

import java.util.concurrent.CompletableFuture;

import com.uulen.ordermanagement.orderservice.dto.commands.OrderCreateDTO;

public interface OrderCommandService {

    public CompletableFuture<String> createOrder(OrderCreateDTO orderCreateDTO);

}
