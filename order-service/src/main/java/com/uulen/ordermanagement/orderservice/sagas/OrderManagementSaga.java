package com.uulen.ordermanagement.orderservice.sagas;

import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.modelling.saga.EndSaga;
import org.axonframework.modelling.saga.SagaEventHandler;
import org.axonframework.modelling.saga.SagaLifecycle;
import org.axonframework.modelling.saga.StartSaga;
import org.axonframework.spring.stereotype.Saga;

import javax.inject.Inject;

import com.uulen.ecommerce.commands.CancelOrderCommand;
import com.uulen.ecommerce.commands.CreateInvoiceCommand;
import com.uulen.ecommerce.commands.CreateShippingCommand;
import com.uulen.ecommerce.commands.UpdateOrderStatusCommand;
import com.uulen.ecommerce.events.*;
import com.uulen.ordermanagement.orderservice.aggregates.OrderStatus;

import java.util.UUID;

@Saga
public class OrderManagementSaga {

    @Inject
    private transient CommandGateway commandGateway;

    @StartSaga
    @SagaEventHandler(associationProperty = "orderId")
    public void handle(OrderCreatedEvent orderCreatedEvent){
        String paymentId = UUID.randomUUID().toString();
        System.out.println("Saga invoked");

        //associate Saga
        SagaLifecycle.associateWith("paymentId", paymentId);

        System.out.println("order id" + orderCreatedEvent.orderId);

        //send the commands
        commandGateway.send(new CreateInvoiceCommand(paymentId, orderCreatedEvent.orderId));
    }

    @SagaEventHandler(associationProperty = "paymentId")
    public void handle(InvoiceCreatedEvent invoiceCreatedEvent){
        String shippingId = UUID.randomUUID().toString();

        System.out.println("Saga continued");

        //associate Saga with shipping
        SagaLifecycle.associateWith("shipping", shippingId);

        //send the create shipping command
        try{
            commandGateway.sendAndWait(new CreateShippingCommand(shippingId, invoiceCreatedEvent.orderId, invoiceCreatedEvent.paymentId));
        }
        catch(Exception e){
            commandGateway.sendAndWait(new CancelOrderCommand(invoiceCreatedEvent.orderId,"CANCELED"));
            e.printStackTrace();
        }
        
    }

    @SagaEventHandler(associationProperty = "orderId")
    public void handle(OrderShippedEvent orderShippedEvent){
        //commandGateway.send(new UpdateOrderStatusCommand(orderShippedEvent.orderId, String.valueOf(OrderStatus.SHIPPED)));
    }

    @SagaEventHandler(associationProperty = "orderId")
    @EndSaga
    public void handle(OrderCancelledEvent event) {// sagagiin tubshind bas event handling hiih yostoi
        System.out.println("OrderCancelledEvent in Saga for Order Id : {} "+
                event.getOrderId());
    }    

    @SagaEventHandler(associationProperty = "orderId")
    @EndSaga    
    public void handle(OrderUpdatedEvent orderUpdatedEvent){
        System.out.println("saga shippend and updated status");
        SagaLifecycle.end();
    }
}
