package com.uulen.shippingmanagement.shippingservice.events;
import com.uulen.ecommerce.events.OrderCancelledEvent;
import com.uulen.ecommerce.events.OrderCreatedEvent;
import com.uulen.ecommerce.events.OrderShippedEvent;

import org.axonframework.eventhandling.EventHandler;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class OrderEventsHandler {

    

    @Autowired
    private JdbcTemplate template;

    @EventHandler
    public void on(OrderShippedEvent event) {
        

        String insertSQL="insert into test_shipment (shipping_id,payment_id,order_number) "+
         " values (?,?,?)";



        template.update(insertSQL,new Object[]{event.shippingId,event.paymentId,event.orderId});
        System.out.println("db must insert here ");
        // insert new event
    }

    

    
}
