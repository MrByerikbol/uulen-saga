package com.uulen.ordermanagement.orderservice.aggregates;

public enum ItemType {

    LAPTOP, HEADPHONE, SMARTPHONE
}
