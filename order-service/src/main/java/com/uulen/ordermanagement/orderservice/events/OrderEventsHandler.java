package com.uulen.ordermanagement.orderservice.events;
import com.uulen.ecommerce.commands.UpdateOrderStatusCommand;
import com.uulen.ecommerce.events.OrderCancelledEvent;
import com.uulen.ecommerce.events.OrderCreatedEvent;
import com.uulen.ecommerce.events.OrderShippedEvent;
import com.uulen.ecommerce.events.OrderUpdatedEvent;

import org.axonframework.eventhandling.EventHandler;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class OrderEventsHandler {

    

    @Autowired
    private JdbcTemplate template;

    @EventHandler
    public void on(OrderCreatedEvent event) {
        

        String insertSQL="insert into test_order (order_status,order_number ) "+
         " values (?,?)";
        template.update(insertSQL,new Object[]{event.orderStatus,event.orderId});
        System.out.println("db must insert here ");
        // insert new event
    }

    @EventHandler
    public void on(OrderCancelledEvent event) { // roll back ch hiij bolno
        String updateSQL="update test_order set order_status=? where order_number = ?";
        template.update(updateSQL,new Object[]{event.orderStatus,event.orderId});
        //rolbacking here 
        System.out.println(" ymar negen baidlaar uur service uuded aldaa garsan  ");
        
    }

    @EventHandler
    public void on(UpdateOrderStatusCommand event) { // roll back ch hiij bolno
        String updateSQL="update test_order set order_status=? where order_number = ?";
        template.update(updateSQL,new Object[]{event.orderStatus,event.orderId});
        //rolbacking here 
        System.out.println(" za ingeed amar saihandaa jargajee  ");
        
    }

    
}
