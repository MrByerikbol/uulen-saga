package com.uulen.shippingmanagement.shippingservice.aggregates;

import com.uulen.ecommerce.commands.CreateShippingCommand;
import com.uulen.ecommerce.commands.UpdateOrderStatusCommand;
import com.uulen.ecommerce.events.OrderShippedEvent;
import com.uulen.ecommerce.events.*;

import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;
import java.lang.Thread;

@Aggregate
public class ShippingAggregate {

    @AggregateIdentifier
    private String shippingId;

    private String orderId;

    private String paymentId;

    public ShippingAggregate() {
    }

    @CommandHandler
    public ShippingAggregate(CreateShippingCommand createShippingCommand){
        AggregateLifecycle.apply(new OrderShippedEvent(createShippingCommand.shippingId, createShippingCommand.orderId, createShippingCommand.paymentId));
    }

    @EventSourcingHandler
    protected void on(OrderShippedEvent orderShippedEvent){ 
        try{
            System.out.println("SHIPMENT SERVICE REQUEST IS CAME");
            int a = 1;
            int b=1;
            
            Thread.sleep(8000);
            
            System.out.println("end irj bna");
            if(a==b){
                AggregateLifecycle.apply(new UpdateOrderStatusCommand(orderShippedEvent.orderId, String.valueOf("SHIPPED")));
            }
            else{
                AggregateLifecycle.apply(new OrderCancelledEvent(orderShippedEvent.orderId,"CANCELED"));
            }
            
            this.shippingId = orderShippedEvent.shippingId;
            this.orderId = orderShippedEvent.orderId;
        }
        catch(Exception e){
            try{
                Thread.sleep(9000);
                //AggregateLifecycle.apply(new OrderCancelledEvent(orderShippedEvent.orderId,"CANCELED"));
            }
            catch(Exception e2){
                e2.printStackTrace();
            }
            

        }
        
    }


    // @CommandHandler
    // public ShippingAggregate(CreateShippingCommand createShippingCommand){
    //     AggregateLifecycle.apply(new OrderShippedEvent(createShippingCommand.shippingId, createShippingCommand.orderId, createShippingCommand.paymentId));
    // }



}
