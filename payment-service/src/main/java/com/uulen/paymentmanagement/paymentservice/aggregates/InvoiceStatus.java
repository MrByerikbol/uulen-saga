package com.uulen.paymentmanagement.paymentservice.aggregates;

public enum InvoiceStatus {

    PAID, PAYMENT_REVERSED
}
