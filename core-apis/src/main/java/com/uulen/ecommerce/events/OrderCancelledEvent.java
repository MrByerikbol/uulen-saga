package com.uulen.ecommerce.events;

public class OrderCancelledEvent {
    public final String orderId;

    public final String orderStatus;

    public OrderCancelledEvent(String orderId, String orderStatus) {
        this.orderId = orderId;
        this.orderStatus = orderStatus;
    } 





    public String getOrderId() {
        return this.orderId;
    }


    public String getOrderStatus() {
        return this.orderStatus;
    }

}