package com.uulen.ordermanagement.orderservice.aggregates;

import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;

import java.math.BigDecimal;

import com.uulen.ecommerce.commands.CreateOrderCommand;
import com.uulen.ecommerce.commands.UpdateOrderStatusCommand;
import com.uulen.ecommerce.commands.CancelOrderCommand;
import com.uulen.ecommerce.events.OrderCancelledEvent;
import com.uulen.ecommerce.events.OrderCreatedEvent;
import com.uulen.ecommerce.events.OrderUpdatedEvent;

@Aggregate
public class OrderAggregate {

    @AggregateIdentifier
    private String orderId;

    private ItemType itemType;

    private BigDecimal price;

    private String currency;

    private OrderStatus orderStatus;

    public OrderAggregate() {
    }

    @CommandHandler
    public OrderAggregate(CreateOrderCommand createOrderCommand){
        AggregateLifecycle.apply(new OrderCreatedEvent(createOrderCommand.orderId, createOrderCommand.itemType,
                createOrderCommand.price, createOrderCommand.currency, createOrderCommand.orderStatus));
    }

    @EventSourcingHandler
    protected void on(OrderCreatedEvent orderCreatedEvent){
        this.orderId = orderCreatedEvent.orderId;
        this.itemType = ItemType.valueOf(orderCreatedEvent.itemType);
        this.price = orderCreatedEvent.price;
        this.currency = orderCreatedEvent.currency;
        this.orderStatus = OrderStatus.valueOf(orderCreatedEvent.orderStatus);
    }

    @CommandHandler
    protected void on(UpdateOrderStatusCommand updateOrderStatusCommand){
        AggregateLifecycle.apply(new OrderUpdatedEvent(updateOrderStatusCommand.orderId, updateOrderStatusCommand.orderStatus));
    }

    @EventSourcingHandler
    protected void on(OrderUpdatedEvent orderUpdatedEvent){
        this.orderId = orderId;

        this.orderStatus = OrderStatus.valueOf(orderUpdatedEvent.orderStatus);
    }

    @CommandHandler
    protected void on(CancelOrderCommand cancelOrderCommand){
        AggregateLifecycle.apply(new OrderUpdatedEvent(cancelOrderCommand.orderId,cancelOrderCommand.orderStatus));    
    }


    @EventSourcingHandler
    protected void on(OrderCancelledEvent orderCancelledEvent){
        this.orderId = orderId;
        this.orderStatus = OrderStatus.valueOf(orderCancelledEvent.orderStatus);
    }



    

}
