package com.uulen.ecommerce.commands;

import org.axonframework.modelling.command.TargetAggregateIdentifier;

public class CancelOrderCommand {
    @TargetAggregateIdentifier
    public final String orderId;

    public final String orderStatus;

    public CancelOrderCommand(String orderId, String orderStatus) {
        this.orderId = orderId;
        this.orderStatus = orderStatus;
    }    
}
